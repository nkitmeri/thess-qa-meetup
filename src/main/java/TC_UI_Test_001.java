import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.io.File;

public class TC_UI_Test_001 {

    @Test
    public void test() throws InterruptedException {

        File file = new File("src/main/resources");

        System.setProperty("webdriver.chrome.driver",
                file.getAbsolutePath() +"/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("http://www.google.com");
        Thread.sleep(5000);
        driver.quit();
    }
}